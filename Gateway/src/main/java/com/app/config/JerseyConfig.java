package com.app.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import com.app.gateway.GatewayService;

@Component
public class JerseyConfig extends ResourceConfig
{
	public JerseyConfig()
	{
		register(GatewayService.class);
	}
}