package com.app.gateway;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.app.shared.event.EventAction;
import com.app.shared.model.Payload;
import com.app.shared.util.FileUtil;

@Component
public class GatewayManager 
{
	private final static String COMPONENT_CONFIG_FILE_NAME = "componentconfig.properties";
	static Map<Object, Object> componentConfig = new HashMap<>();

	static
	{
		componentConfig = new FileUtil().getPropertiesFileData(COMPONENT_CONFIG_FILE_NAME);
	}

	public Payload emitEvent(Payload payload) 
	{
		try 
		{
			EventAction event = (EventAction) Class.forName(componentConfig.get(payload.getName()).toString()).newInstance();
			payload = event.execute(payload);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return payload;
	}


}
