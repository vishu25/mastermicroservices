package com.app.gateway;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.app.shared.model.Payload;

@RestController
@Path("/gateway")
public class GatewayService
{
	@Autowired()
	GatewayManager gatewayManager;
	
	@GET
	public Payload get()
	{
		return null;
	}

	@POST
	public Payload post(Payload payload)
	{
		System.out.println("Post");
		return gatewayManager.emitEvent(payload);
	}

	@PUT
	public Payload put(Payload payload)
	{
		System.out.println("Put");
		return null;
	}

	@DELETE
	public Payload delete(Payload payload)
	{
		System.out.println("Delete");
		return null;
	}

}
