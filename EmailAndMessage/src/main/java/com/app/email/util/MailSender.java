package com.app.email.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

@Component("javaMailSender")
public class MailSender 
{
	MimeMessage message;
	MimeMessageHelper helper;

	@Autowired
	JavaMailSender sender;

	@Autowired
	private VelocityEngine velocityEngine;



	public MailSender bind(String subject,String body)
	{
		try
		{
			this.message = sender.createMimeMessage();
			this.helper = new MimeMessageHelper(message, true);
			this.helper.setSubject(subject);

			if(body != null && body != "")
				this.helper.setText(body);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return this;
	}

	public MailSender template(String templateLocation,Map<String,Object> data)
	{
		try
		{
			String text = VelocityEngineUtils.mergeTemplateIntoString(this.velocityEngine, templateLocation , data);
			this.helper.setText(new StringBuffer().append(text).toString(), true);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return this;
	}

	public MailSender attachment(File file)
	{
		try
		{
			//			this.helper = new MimeMessageHelper(this.message,true);
			this.helper.addAttachment(file.getName(), file);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return this;
	}

	public boolean send(String[] to, String[] cc, String[] bcc)
	{
		boolean isSend = false;
		try
		{
			this.helper.setTo(to);
			if(cc != null)
				this.helper.setCc(cc);
			if(bcc != null)
				this.helper.setBcc(bcc);

			this.sender.send(this.message);
			isSend = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return isSend;
	}


	//	@Autowired
	//	MailSender mailSender;
	public static void main(String[] args)
	{
		String[] to = {"arvind.kumar@absolutdata.com","vishuraj2511@gmail.com"};
		Map<String,Object> data = new HashMap<>();
		data.put("name", "vishu raj");
		//		Email email = new Email(to,null, null, "Test Subject","Hello i am here", false, "Today Task", new File("C:\\Users\\arvind.kumar\\Desktop\\TodayTask.xlsx"), true,"index.html", data);
		//		mailSender.send(email);


		/**String[] to = {"arvind.kumar@absolutdata.com"};
		mailSender.bind("Forget Password",null);
		Map<String,Object> data = new HashMap<>();
		data.put("name","vishu");
		mailSender.template("index.html", data);
//		mailSender.attachment(new File("C:\\Users\\arvind.kumar\\Desktop\\TodayTask.xlsx"));
		mailSender.send(to, null, null); */
	}





}


