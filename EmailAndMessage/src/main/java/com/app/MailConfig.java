package com.app;

import java.io.IOException;
import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;

@Configuration
@EnableTransactionManagement
@PropertySource("classpath:mail.properties")
public class MailConfig
{
	@Value("${spring.mail.host}")
	private String HOST;

	@Value("${spring.mail.port}")
	private int PORT;

	@Value("${spring.mail.username}")
	private String USERNAME;

	@Value("${spring.mail.password}")
	private String PASSWORD;

	@Value("${spring.mail.properties.mail.smtp.auth}")
	private String AUTH;

	@Value("${spring.mail.properties.mail.smtp.starttls.enable}")
	private String START_TLS_ENABLE;

	@Bean
	public JavaMailSenderImpl javaMailSenderImpl() 
	{
		JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
		javaMailSender.setPort(587);
		javaMailSender.setHost(HOST);
		javaMailSender.setUsername(USERNAME);
		javaMailSender.setPassword(PASSWORD);
		Properties javaMailProperties = new Properties();
		javaMailProperties.put("mail.smtp.starttls.enable", START_TLS_ENABLE);
		javaMailProperties.put("mail.smtp.auth", AUTH);
		javaMailProperties.put("mail.transport.protocol", "smtp");
		javaMailProperties.put("mail.debug", "true");
		javaMailSender.setJavaMailProperties(javaMailProperties);

		return javaMailSender;
	}
	
	@Bean
	public VelocityEngine velocityEngine() throws VelocityException, IOException{
		VelocityEngineFactoryBean factory = new VelocityEngineFactoryBean();
		Properties props = new Properties();
		props.put("resource.loader", "class");
		props.put("class.resource.loader.class", 
				  "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		factory.setVelocityProperties(props);
		
		return factory.createVelocityEngine();
	}

}
