package com.app.shared.model;

import java.util.Map;

public class Payload
{
	private String key;
	private String name;
	private String action;
	private Map<String,Object> data;
	private String ipAddress;
	private Response response;

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Map<String, Object> getData() {
		return data;
	}
	public void setData(Map<String, Object> data) {
		this.data = data;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public class Response
	{
		private String status;
		private int code;
		private Map<String,Object> body;

		public Response(String status, int code, Map<String, Object> body)
		{
			super();
			this.status = status;
			this.code = code;
			this.body = body;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public int getCode() {
			return code;
		}
		public void setCode(int code) {
			this.code = code;
		}
		public Map<String, Object> getBody() {
			return body;
		}
		public void setBody(Map<String, Object> body) {
			this.body = body;
		}

	}

	public Response getResponse() {
		return response;
	}
	public void setResponse(Response response) {
		this.response = response;
	}
}
