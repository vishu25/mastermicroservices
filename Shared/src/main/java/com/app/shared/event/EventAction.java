package com.app.shared.event;

import com.app.shared.model.Payload;

public interface EventAction
{
	public Payload execute(Payload payload);
}
