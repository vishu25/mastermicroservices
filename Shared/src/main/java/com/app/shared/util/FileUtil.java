package com.app.shared.util;

import java.util.Properties;

import org.springframework.core.io.ClassPathResource;

public class FileUtil
{
	public Properties getPropertiesFileData(String fileName)
	{
		Properties properties = new Properties();
		try
		{
			ClassPathResource resource = new ClassPathResource(fileName);
			properties.load(resource.getInputStream());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return properties;
	}
}
