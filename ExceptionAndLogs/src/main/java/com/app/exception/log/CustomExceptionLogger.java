package com.app.exception.log;

import org.apache.log4j.Logger;

public class CustomExceptionLogger extends Exception 
{
	private static final long serialVersionUID = 1L;

	static Logger logger = Logger.getLogger(CustomExceptionLogger.class);

	public CustomExceptionLogger(Class<?> className)
	{
//		super(message);
		logger = Logger.getLogger(className);
	}

	public void error(Exception exception)
	{
		logger.info("====================== START =======================");
		logger.info("Cause : "+exception.getClass().getCanonicalName());
		logger.info("Message : "+exception.getMessage());
		logger.info("Class : "+exception.getStackTrace()[0].getClassName());
		logger.info("Method : "+exception.getStackTrace()[0].getMethodName());
		logger.info("Line : "+exception.getStackTrace()[0].getLineNumber());
		logger.error("Exception Stack Traced",exception);
		logger.info("====================== END =======================");
	}

	public void error(String message, Exception exception)
	{
		logger.error(message,exception);
	}

	public void info(String message, Exception exception)
	{
		logger.info(message);
	}

	public void warning(String message, Exception exception)
	{
		logger.warn(message);
	}

	public void fatal(String message, Exception exception)
	{
		logger.fatal(message);
	}

	public void debug(String message, Exception exception)
	{
		logger.debug(message);
	}

}
