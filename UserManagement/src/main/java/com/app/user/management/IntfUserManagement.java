package com.app.user.management;

import com.app.shared.event.EventAction;
import com.app.shared.model.Payload;

public interface IntfUserManagement extends EventAction
{
	Payload saveUser(Payload payload);
	Payload getUserById(Payload payload);
	Payload getUserByEmail(Payload payload);
	Payload updateUser(Payload payload);
	Payload deleteUser(Payload payload);
	Payload getUserList();

}
